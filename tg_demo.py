import torch
import torch_geometric
from torch_geometric.data import Data
from torch_geometric.datasets import TUDataset
dataset =TUDataset(root='/home/et/zakk/ENZYMES',name='ENZYMES')

edge_index = torch.tensor([[0, 1, 1, 2],
                           [1, 0, 2, 1]], dtype=torch.long)
x = torch.tensor([[-1], [0], [1]], dtype=torch.float)

data = Data(x=x, edge_index=edge_index)
print("dataset.num_classes: ",dataset.num_classes)
print("data.is_undirected(): ",data.is_undirected())
train_dataset=dataset[:540]
test_dataset=dataset[540:]
dataset = dataset.shuffle()

from torch_geometric.datasets import KarateClub as kcd



