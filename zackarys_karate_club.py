import torch
import torch.nn as nn
import numpy as np
import sys
import os
sys.path.append("/home/et/flow/ftools/")
sys.path.append(os.getcwd())
import matplotlib.pyplot as plt
import torch.optim as optz

best_list = []
if True:
    class GCN(nn.Module):
        def __init__(self):
            super(GCN, self).__init__()
            self.f1 = nn.Linear(34,1)
            self.act = nn.Hardtanh()
        def forward(self,x):
            #return self.f1(x)
            return self.act(self.f1(x))
    
    
    ## Get social-graph 
    import networkx as nx
    kg = nx.karate_club_graph()
    nodes_l = list(kg.nodes())
    
    ## Parse social-graph into adjacency matrix.
    sparse_adj = [list(kg.adj[i]) for i in nodes_l]
for x in range(100):
    adj = np.zeros((34,34),dtype=np.float32)
    for i,o in enumerate(sparse_adj):
        adj[i][np.array(o)] = 1
    adj = np.matrix(adj,dtype=np.float32)                    #Adjacency matrix
    deg = np.array(np.sum(adj,axis=0))[0]   #Degree matrix.
    
    
    ## Attraction/Repulsion weights
    I = np.eye(len(adj),dtype=np.float32) #Identity
    Ah = np.float32(2*adj-1+I) #Times 2 minus one, then plus ones along diagonal
    
    ## Attraction/Repulsion weight matrix
    #   > has zeros along the diagonal:                                 points not attracted to themselves
    #   > has 1 for indices to which (the row point) is connected:      connected points are attracted
    #   > has -1 for for incideces to which it  is not connected:       non-connected points are repelled
    ##
    
    ## Normalizing the Feature Representations [1]: Inputs
    #Ah = adj+I
    #~" f(X,A) = (D**-1)A*X
    #   D -  Degreee matrix
    #   A -  Adjacency
    #   X -  Feature matrix
    
    ##  Degree matrix of A_hat: D_hat ~ dh
    #       dh = np.array(np.sum(adj,axis=0))[0]
    #       dhi = np.matrix(np.diag(dh))**-1 #inverse degree_hat matrix
    #       dhi = torch.FloatTensor(dhi.tolist())
    
    ## Simple normalization of attraction matrix.
    Aj = Ah + np.matrix(np.diag(deg))
    #Aj = np.array(Ah + np.matrix(np.diag(deg)),dtype=np.float32)
    
    dh = np.array(np.sum(Ah,axis=0),dtype=np.float32)[0] ## straight from article
    dh = dh + (dh==0)
    dhi = np.matrix(np.diag(dh))**-1 #diagonal matrix normalizes row-wise.
    dhi = torch.FloatTensor(torch.from_numpy(dhi))
    
    
    
    #Ah = np.array(Ah,dtype=np.float32)
    Ah = torch.from_numpy(Ah)
    #print(Ah)
    #del Ah
    
    #Aj = np.array(Aj,dtype=np.float32)
    #Aj -= np.min(Aj)
    #Aj /= Aj.flatten()[np.argmax(np.abs(Aj))]
    #Aj *= 2
    #Aj -= 1
    Aj = torch.from_numpy(Aj)
    
    inda = torch.matmul(dhi,Ah) #inda is the normalized attraction matrix
    
    print(x)
    
    net = GCN()
    opr = optz.SGD(net.parameters(),lr=0.00002,momentum=0.000000000000001) 
    
    total_epochs = 1500
    N_checkpoints = 20
    N_epoch_chunks = total_epochs//(N_checkpoints+1)
    
    
    norming_adj_m = np.array(np.sum(adj,axis=0),dtype=np.float32)[0] ## straight from article
    norming_adj_m = np.matrix(np.diag(norming_adj_m))**-1 #diagonal matrix normalizes row-wise.
    norming_adj_m = torch.FloatTensor(torch.from_numpy(norming_adj_m))
    adj = torch.FloatTensor(torch.from_numpy(np.array(adj,dtype=np.float32)))
    
    normd_adj = np.matmul(norming_adj_m,adj)
    
    for checkpoint_ind in range(N_checkpoints):
        #outs = net(inda)
        #plt.figure()
        #plt.title("Clustering after "+str(checkpoint_ind*N_epoch_chunks)+" epochs")
        #plt.xlabel("Dim 1")
        #plt.ylabel("Dim 1")
        #plt.scatter(outs[:,0].tolist(),outs[:,0].tolist())
    
        for epoch in range(N_epoch_chunks):
            #outs = net(normd_adj)
            outs = net(inda)
            loss = torch.sum((outs-outs.T)**2*Ah)#,axis=0)
            #loss = torch.sum(l1)
    
            ##  Loss function composition
            #     > Distance from each point to every other point: outs-outs.T
            #     > Squared: (outs-outs.T)**2
            #     > Multiplied by an attraction/repulsion matrix: (outs-outs.T)**2*Ah
            #     > Summed per node, then together.
            #
            ##  Loss function optimizes for
            #     > Minimizes/maximizes the distance between attracted/repulsed nodes
            #     > Splits group in two.
            #
            ##
    
            loss.backward()
            opr.step()
            #net.zero_grad() #Doesn't work with zeroing grad
        #print(loss.item())
    
    #plt.figure()
    #plt.title("Finished training")
    #plt.xlabel("Dim 1")
    #plt.ylabel("Dim 1")
    #plt.scatter(outs[:,0].tolist(),outs[:,0].tolist())
    ##plt.show()
    #plt.close()
    
    
    ## Auto-catagorizing observed catagories.
    k = torch.sign(outs).detach().numpy()
    ksl,ksl2,k2 = np.unique(k,axis=0,return_index=True,return_inverse=True)
    final_predictions = np.argsort(np.argsort(ksl2))[k2]
    
    ## Get labels.
    nx_labels = [kg.nodes[i]["club"] for i in nodes_l]
    clubs_set = list(set(nx_labels))
    clubs_set.sort()
    club_labels = [clubs_set.index(x) for x in nx_labels]
    
    
    #print(final_predictions.tolist())
    #print(club_labels)
    ## Number of incorrect predictions:
    #print("number of incorrect predictions: ",
    incorr = np.where(final_predictions != np.array(club_labels))[0].shape[0]
    #print("number of inverse incorrect predictions: ",
    inv_incorr = np.where(np.abs(final_predictions-1)!= np.array(club_labels))[0].shape[0]
    best_list.append(min(incorr, inv_incorr))

print(np.mean(best_list))
