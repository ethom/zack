#
#generates adjacency matrix?
def libga(G):
    mg = max(max(G))+1
    M = [[0]*mg for i in range(mg)]
    for i in G:
        M[i[0]][i[1]] = 1
        M[i[1]][i[0]] = 1
    return M
