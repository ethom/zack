import torch
def class_sort_gen(outs):
    # creates general sorting for binary code outputs.
    #   Networks' outputs will determine the number of classes.A
    claz = list(set([str(i) for i in torch.round(outs).tolist()]))
    unlabd = [claz.index(j) + 1 for j in [str(i) for i in torch.round(outs).tolist()]] # doesn't label in order.
    ordering = [(i,unlabd.index(i)) for i in list(set(unlabd))]   #yeilds tuple with (set_instance, first index)
    ordering.sort(key=lambda ordering:ordering[1])  #sorts ordering according to first instance
    sortd = [0 for i in range(len(unlabd))]
    for i,oi in enumerate(ordering):            #Enumerating the ordering {classes}
        for j in range(unlabd.count(oi[0])):    #Finds index exactly the number of time that there are
                                                    #in the list
            k = unlabd.index(oi[0])             #find index of unlabd_class instance
            sortd[k] = i + 1 #find first index of ordering_object_i[0] {the class}
                                                    #plus one so starts with class 1
            unlabd[k] = "X" #remove index from unlabd
    return sortd

#ots = torch.Tensor([[1.0, -1.0], [1.0, -1.0], [1.0, -1.0], [1.0, -1.0], [0.9906036257743835, -0.9830163717269897], [0.9872725009918213, -0.9155648946762085], [0.9869171380996704, -0.9201114177703857], [0.9999914765357971, -0.9999997019767761], [-0.9598057866096497, -0.7664358019828796], [-0.9982119798660278, 0.916333019733429], [0.9905933737754822, -0.9841444492340088], [0.9639338254928589, -0.9902423620223999], [0.9980693459510803, -0.9996436238288879], [0.9990299344062805, -0.9999930262565613], [-0.999958872795105, 0.9978813529014587], [-0.9999585151672363, 0.9978488683700562], [-0.7868135571479797, 0.8718054294586182], [0.9990764856338501, -0.9998447895050049], [-0.999957799911499, 0.9978997111320496], [0.9270935654640198, -0.9961560964584351], [-0.9999580383300781, 0.9978675246238708], [0.9990970492362976, -0.9998416304588318], [-0.9999585747718811, 0.9978442192077637], [-1.0, 0.9999988079071045], [-0.9991329908370972, 0.9938219785690308], [-0.9998313784599304, 0.9986521601676941], [-0.9995540976524353, 0.9920802712440491], [-0.9999978542327881, 0.9998046159744263], [-0.9997006058692932, 0.9832617044448853], [-0.9999987483024597, 0.9999264478683472], [-0.9990167617797852, 0.8030597567558289], [-0.9999985098838806, 0.9997960329055786], [-1.0, 1.0], [-0.9994925260543823, 0.9993095397949219]])
#dum = class_sort_gen(ots)
#print(dum)

