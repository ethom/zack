import torch
import torch.nn as nn
import numpy as np
import sys
sys.path.append("/home/et/flow/ftools/")
from libga import *
from class_sort import *
import matplotlib.pyplot as plt
import torch.optim as optz

class GCN(nn.Module):
    def __init__(self):
        super(GCN, self).__init__()
        self.f1 = nn.Linear(34,2)
    def forward(self,x):
        return torch.tanh(self.f1(x))

graphr_file = open("zach_karate_club",'r')
graphrs = graphr_file.read()
graphr_file.close()

graphrs = graphrs[2:-3].split("],[")
graphrs = [list(map(int,i.split(","))) for i in graphrs]
graphrs =[[i-1 for i in j] for j in graphrs]
adj = np.matrix(libga(graphrs))
X = np.eye(len(adj),dtype=np.float32)
#Ah = adj+X
Ah = 2*adj-1+X
dh = np.array(np.sum(Ah,axis=0))[0]
Ah = np.array(Ah,dtype=np.float32)
dhi = np.matrix(np.diag(dh))**-1
dhi = torch.FloatTensor(dhi.tolist())
ah = torch.from_numpy(Ah)
#ah = -(ah*2-1)
print(ah)
X = torch.from_numpy(X)
outl = []

for dudii in range(4):
    net = GCN()
    opr = optz.SGD(net.parameters(),lr=0.00002,momentum=0.000000) 
    inda = torch.matmul(torch.matmul(dhi,ah),X)#.reshape((1,1,34**2))
    for dudi in range(5):
        for dud in range(100):
            outs = net(inda)
            #l1 = [torch.sum(((outs[:,0]-outs[:,0][i])**2+(outs[:,1]-outs[:,1][i])**2)*ah[i]) for i,o in enumerate(outs[:,0])]
            l1 = [torch.sum(((outs[:,0]-outs[:,0][i])**2+(outs[:,1]-outs[:,1][i])**2)*ah[i]) for i,o in enumerate(outs[:,0])]
            loss = sum(l1)
            loss.backward()
            opr.step()
        #plt.scatter(outs[:,0].tolist(),outs[:,1].tolist())
        #print(loss.item())
    j=(outs+1).tolist()
    l=np.round([j[i][0]*2+j[i][1] for i in range(len(j))])
    #print(cals(l))
print(l)
    #outl.append(np.sign((outs[0][0]*outs).tolist()).flatten().tolist())
#outs = np.array(outs.T.tolist())
#o = np.array(outl)
#for i in range(20):
#	print(set(o[:,i].tolist()),[o[:,i].tolist().count(j) for j in set(o[:,i].tolist())])
