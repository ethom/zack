#October 8th 2019, moved from bumblebee fnp8.py to fnp9.py on arcee
import os
file_path = os.path.realpath(__file__)
print(file_path)

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import time
import sys
import os
import shutil
sys.path.append("/home/et/flow/ftools/")
import libadj
import libscoreh5
import libnpaths
import libup
import h5py
import random
import matplotlib.pyplot as plt

transformr = True 
if transformr == True:
    print("Transformer")
    sys.path.append(os.path.abspath("/home/et/flow/ftools/"))
    import torch.cuda as ft
    data_dir = "/home/et/flow/fhash"
    res_dir  = "/home/et/flow/fhash/results/"
else:
    sys.path.append(os.path.abspath("/home/et/flow/ftools/"))
    import torch as ft
    data_dir = "/home/et/flow/fhash"
    res_dir  = "/home/et/flow/fhash/results/"

if torch.cuda.is_available():
    device  = torch.device("cuda")
else:
    device = torch.device("cpu")

def vectr(x):
    l = [0]*6
    l[x] = 1 
    return l


class CNA(nn.Module):
    def __init__(self):
        super(CNA, self).__init__()
        #self.fc0 = nn.Linear(28*28,6*28*28)

        self.c1 = nn.Conv2d(1, 16, 2,stride=1)
        self.c2 = nn.Conv2d(16,32, 2,stride=2)

        self.encoder = nn.Sequential(
        #        nn.Hardtanh(),
                self.c1, #nn.Conv2d(3, 6, 5,stride=1,padding=16)
		nn.Tanh(),	
                self.c2, #nn.Conv2d(6,16,5,stride=1),
                nn.Tanh())
        self.fcl = nn.Linear(784**2**32,80)
        self.fc2 = nn.Linear(80,6)
    def forward(self,x):
        #print("in: ",x.shape)
        #x = ((self.fc0(x))).reshape((-1,6,28,28))
        #print("upscale: ",x.shape)
        x = self.encoder(x)
        #print("encoder: out",x.shape)
        x = torch.tanh(self.fcl(x.view(-1,784**2**32)))
        x = torch.relu(self.fc2(x))
        #print("out: ",x.shape)
        return x
epoch_act = 0
act_time = time.time()
with h5py.File("/home/et/flow/fhash/np100k28_nn.hdf5",'r') as hf:

    AvgErr_v_TA = []
    Avgpred_v_TA = []
    nl_l = []
    wl_l = []
    RelErr_v_TA = []
    slt_bw = []
    hg_lcs = []


    AvgErr_v_TA2 = []
    Avgpred_v_TA2 = []
    nl_l2 = []
    RelErr_v_TA2 = []
    slt_bw2 = []
    hg_lcs2 = []
    wllow_l = []
    xlrl = []

    os.chdir(res_dir)
    result_class = file_path.split("/")[-1][:-3] #first: what file are results from
    result_number = str(len(os.listdir(res_dir))) #random counter
    os.mkdir(str(result_class+"_"+result_number))
    os.chdir(str(result_class+"_"+result_number))
    dest_path = str(os.getcwd()+"/"+result_class+".py")

    shutil.copyfile(file_path,dest_path)

    hp = hf['p']
    hash_list= list(hp.keys())
    mkl = [list(hp[x].keys())[:1000] for x in hash_list]
    fn = np.shape(hp[list(hp.keys())[0]][mkl[0][0]][:])[0]
    M = torch.HalfTensor(torch.from_numpy(np.float16(libadj.main(fn))))

    for xvlad in hash_list:
        lowloss = 100000
        lossthreshflag = True
        net = nn.DataParallel(CNA())
        net.half()
        net.to(device)

        epoch_size = sum([len(x) for x in mkl])
        batch_mult = 512
        epoch_mult = 350
        egroup_n = 10

        m0 = 0.00008
        lrp = 0.008*5#0.00175*150
        mxlr = lrp*2
        milr = lrp/10#3
        lrmult = ((mxlr-milr)/2)
        lradd = ((mxlr-milr)/2)+milr
        

        optimizer = optim.SGD(net.parameters(),lr = lrp,momentum=m0/10)
        criterion = nn.CrossEntropyLoss()
        lcl = []
        hash_list = list(hp.keys())
        test_group = hash_list.pop(int(xvlad))
        train_group = hash_list[:]
        epochchunk = int(epoch_mult/egroup_n)
        epoch_loss_list = []
        lrl = []
        for egroup in range(egroup_n):
            eg_loss = 0
            epoch_loss = 0
            avgel = 0.2
            for epoch in range(epochchunk):
                epoch_act += 1
                #for g in optimizer.param_groups:
                #    lrdum = min(1,avgel)*(np.exp(np.log(0.85)/10)**egroup)*(lrmult*((int((epoch+1)/epochchunk))%3)+lradd)
                #    #lrdum = (epoch_loss/epoch_size)*(np.exp(np.log(0.5)/10)**egroup)*(lrmult*np.cos((np.pi*int(epoch)/epochchunk)%3)+lradd)
                #    lrl.append(lrdum)
                #    g['lr'] = lrdum

                epoch_loss = 0
                etime_b = time.time()
                for train_group in hash_list:
                    tg_loss = 0
                    tgind = int(train_group)
                    random.shuffle(mkl[tgind])
                   #for i,o in enumerate(mkl):
                   #    random.shuffle(mkl[i])
                    batch_size = int(len(mkl[tgind])/batch_mult) #Should this be variable?
                    print(batch_size)
                    for batch in range(batch_mult):
                        print(batch)
                        batch_loss= 0
                        if True: #try:
                            bdata = np.array([hp[train_group][mkl[tgind][batch*batch_size+x]][:].flatten() for x in range(batch_size)])
                            bdata = np.array([np.diag(bdata[i]) for i in range(batch_size)])
                            
                            


                            bnp = [hp[train_group][mkl[tgind][batch*batch_size+x]].attrs['npaths'] for x in range(batch_size)]
                            #bnp = [vectr(x) for x in bnp]


                            bs = batch_size
                        else: #except Exception as e: #IndexError:
                            print("ERROR")
                            print(e)
                            print("\n")
                            bdata = []
                            bnp = []
                            runt = 0
                            try:
                                while runt<batch_size:
                                    bdata.append(hp[train_group][mkl[tgind][batch*batch_size+runt]][:])
                                    bnp.append(hp[train_group][mkl[tgind][batch*batch_size+runt]].attrs['npaths'])
                                    runt+=1
                            except Exception as ef: #IndexError:
                                print(ef)
                                bdata = ft.HalfTensor(bdata).reshape((runt,fn*fn))
                                bnp = ft.LongTensor(bnp)
                                print("RUNT: ",runt)
                                bs = runt
                                runt = batch_size+100
                        #bdata = libup.up(fn,bdata)
                        #bdatap = np.zeros((bs,2*fn,fn))
                        #for i in range(fn):
                        #    bdatap[:,2*i,:] = bdata[:,i,:]
                        #    bdatap[:,2*i+1,:] = bdata[:,i,:]
                        #bdatap2 = np.zeros((bs,2*fn,2*fn))

                        #for i in range(fn):
                        #    bdatap2[:,:,2*i] = bdatap[:,:,i]
                        #    bdatap2[:,:,2*i+1] = bdatap[:,:,i]

                        bnp = ft.LongTensor(bnp)
                        bdata = torch.HalfTensor(bdata)#.reshape((bs,1,fn,fn))
                        bdata = torch.matmul(M,bdata).reshape((bs,1,784,784))
                        bdata = bdata.to(device)
                        pred = net(bdata)
                        loss = criterion(pred,bnp)
                        loss.backward()
                        optimizer.step()
                        batch_loss += loss.item() 
                        lcl.append([epoch,tgind,batch,batch_loss/bs])
                        if np.random.uniform() < 0.98:
                            net.zero_grad()
                    tg_loss += batch_loss*bs#*batch_size
                    net.zero_grad()
                epoch_loss += tg_loss
                net.zero_grad()
                etime_e = time.time()-etime_b
                avgel = epoch_loss/epoch_size
                print('%d.%d'%tuple((egroup,epoch)),
                        "\tepoch loss: ",round(epoch_loss,3),
                        "\tepk/x/d: ",int((16*60*60)/etime_e/len(hash_list)),"\tlr: ",
                        np.format_float_scientific(lrdum,precision=3))
                epoch_loss_list.append(avgel)
                if lowloss > avgel:
                    lowloss = avgel
                    torch.save(net.state_dict(),"npaths_pred_lowest_"+str(xvlad)+".pt")
                    if lossthreshflag == True:
                        if epoch_loss <=1000:
                            for g in optimizer.param_groups:
                                #g['lr'] = lrp/10    #Useless.. lr being updated wrt cosfunc ever epoch
                                g['momentum'] = m0*3
                            lossthreshflag = False
                        elif epoch_loss <1200:
                            for g in optimizer.param_groups:
                                #g['lr'] = lrp/5    #Useless.. lr being updated wrt cosfunc ever epoch
                                g['momentum'] = m0*2
            eg_loss += epoch_loss
            #Intervetion if poor performance
            #if lowloss > 0.06:
            #    net = CNA()
            #    optimizer = optim.SGD(net.parameters(),lr = lrp,momentum=m0)
            #    criterion = nn.CrossEntropyLoss()
            #    print("poor performance.. reset")

        bdata = np.array([hp[test_group][x][:] for x in mkl[int(test_group)]])
        batch_size = len(mkl[int(test_group)])
        bdata = bdata.reshape((batch_size,fn,fn))
        bdata = libup.up(fn,bdata)

        #bnp = [hp[train_group][mkl[tgind][batch*batch_size+x]].attrs['npaths'] for x in range(batch_size)]
        bnp = [hp[test_group][x].attrs['npaths'] for x in mkl[int(test_group)]]
        #bnp = [vectr(x) for x in bnp]


        bdata = ft.HalfTensor(bdata).reshape((batch_size,1,2*fn,2*fn))
        bnp = ft.LongTensor(bnp)

        score = libscoreh5.score(mkl[int(test_group)],
                net,hp[test_group],bdata,bnp)
        #Sort performance metrics

        slt = score[0]
        nlt = score[1]
        nrmdsc = score[2]
        avgpred = score[3]
        wl = score[4]

        #Save performance metrics for cross-validation plots.
        slt_bw.append(np.array(slt))
        AvgErr_v_TA.append(nrmdsc)
        Avgpred_v_TA.append(avgpred)
        nl_l.append(nlt)
        lclnp = np.array(lcl)
        hg_lcs.append(epoch_loss_list)
        xlrl.append(lrl)
        wl_l.append(wl)
        
        netlow = nn.DataParallel(CNA())
        netlow.load_state_dict(torch.load("npaths_pred_lowest_"+str(xvlad)+".pt"))
        score2 = libscoreh5.score(mkl[int(test_group)],
                netlow,hp[test_group],bdata,bnp)

        slt2 = score2[0]
        nlt2 = score2[1]
        nrmdsc2 = score2[2]
        avgpred2 = score2[3]
        wl2 = score2[4]
        #Save performance metrics for cross-validation plots.
        slt_bw2.append(np.array(slt2))
        AvgErr_v_TA2.append(nrmdsc2)
        Avgpred_v_TA2.append(avgpred2)
        nl_l2.append(nlt2)
        wllow_l.append(wl2)

        torch.save(net.state_dict(),"npaths_pred_"+xvlad+".pt") 

         

#Plotting
#print("slt_bw: ",len(slt_bw))
#print("AvgErr_v_TA: ",len(AvgErr_v_TA))
#
#plt.figure()
#for i,o in enumerate(AvgErr_v_TA):
#    plt.scatter(slt_bw[i],o)
#plt.legend(list(map(str,range(4))))
#plt.title("Train/Test: Average Error by N-Paths")
#plt.xlabel("N-Paths")
#plt.ylabel("Average Error")
#plt.savefig('AverageError_v_NPaths.png')
#plt.close()
#
#plt.figure()
#for i,o in enumerate(nl_l): # Number of Trials vs. NPaths
#    plt.scatter(slt_bw[i],o)
#plt.legend(list(map(str,range(4))))
#plt.title("Train/Test: Number of Tests by NPaths")
#plt.xlabel("NPaths")
#plt.ylabel("Number of Tests")
#plt.savefig('NumberofTest_v_NPaths.png')
#plt.close()
#
#plt.figure()
#for i,o in enumerate(Avgpred_v_TA): #Average Prediction vs. NPaths
#    plt.scatter(slt_bw[i],o)
#plt.legend(list(map(str,range(4))))
#plt.title("Average Prediction by NPaths")
#plt.xlabel("Number of Paths")
#plt.ylabel("Average Prediction")
#plt.savefig("AveragePrediction_v_NPaths_TG.png")

#Loss Curve
hg_lcs = np.array(hg_lcs)
plt.figure()
for i in range(len(hg_lcs)):
    plt.plot(hg_lcs[i])
plt.legend(list(map(str,range(4))))
plt.title("Cross-Validation Learning Curves")
plt.xlabel("Epoch")
plt.ylabel("Average Loss")
plt.savefig("Cross-Validation_LearningCurves.png")
plt.close()

#Log Loss Curve
try:
    plt.figure()
    for i in range(len(hg_lcs)):
        plt.semilogy(hg_lcs[i])
        #plt.semilogy(i[:,0],i[:,1])
    plt.title("Cross-Validation Log10 Learning Curves")
    plt.xlabel("Epoch")
    plt.ylabel("Average Loss")
    plt.legend(list(map(str,range(4))))
    plt.savefig("Cross-Validation_Log10LearningCurves.png")
    plt.close()

except:
    plt.figure()
    for i in range(len(hg_lcs)):
        plt.semilogy(hg_lcs[i])
        #plt.semilogy(i[1:][:,0],i[:1][:,1])
    plt.title("Cross-Validation Log10 Learning Curves")
    plt.xlabel("Epoch")
    plt.ylabel("Average Loss")
    plt.legend(list(map(str,range(4))))
    plt.savefig("Cross-Validation_Log10LearningCurves.png")
    plt.close()

w = 1/6
x = np.arange(6)

fig,ax = plt.subplots()
for i,o in enumerate(wl_l):
    ts = ax.bar(x+(i-3)*w, o, w)
    tf = ax.bar(x+(i-3)*w, [nl_l[i][x]-o[x] for x in range(len(o))], w, bottom = o, color='k')
    ts.set_label("xv"+str(i))
tf.set_label("fails")
fig.subplots_adjust(bottom=0.3)
plt.legend(ncol=(len(wl_l)+1),bbox_to_anchor=(0,-0.3),loc='lower left')
plt.title("Success Rates per XValidation, per Class")
plt.xlabel("Number of Paths")
plt.ylabel("% Successful")
plt.savefig("SuccessRate.png")

#Lowest loss nets plotting

#plt.figure()
#for i,o in enumerate(AvgErr_v_TA2):
#    plt.scatter(slt_bw2[i],o)
#    #plt.bar(slt_bw[i]+bar_width*(-0.5+i/len(AvgErr_v_TA)),o,width=bar_width/(1.25*len(hash_list)),align='center',edgecolor='k')
#plt.legend(list(map(str,range(4))))
#plt.title("Train/Test: Average Error by N-Paths, Lowest Loss")
#plt.xlabel("N-Paths")
#plt.ylabel("Average Error")
#plt.savefig('AverageError_v_NPathsLow.png')
#plt.close()
#
#plt.figure()
#for i,o in enumerate(nl_l2): # Number of Trials vs. NPaths
#    plt.scatter(slt_bw2[i],o)
#    #plt.bar(slt_bw[i]+bar_width*(-0.5+i/len(nl_l)),o,width=bar_width/(1.25*len(hash_list)),align='center',edgecolor='k')
#plt.legend(list(map(str,range(4))))
#plt.title("Train/Test: Number of Tests by NPaths, Lowest Loss")
#plt.xlabel("NPaths")
#plt.ylabel("Number of Tests")
#plt.savefig('NumberofTest_v_NPathsLow.png')
#plt.close()

#plt.figure()
#for i,o in enumerate(Avgpred_v_TA2): #Average Prediction vs. NPaths
#    plt.scatter(slt_bw2[i],o)
#    #plt.bar(slt_bw[i]+bar_width*(-0.5+i/len(Avgpred_v_TA)),o,width=bar_width/(1.25*len(hash_list)),align='center',edgecolor='k')
#plt.legend(list(map(str,range(4))))
#plt.title("Average Prediction by NPaths, Lowest Loss")
#plt.xlabel("Trinagle Area")
#plt.ylabel("Average Prediction")
#plt.savefig("AveragePrediction_v_NPaths_TGLow.png")

plt.figure()
for i,o in enumerate(xlrl):
    plt.plot(o)
plt.legend(list(map(str,range(4))))
plt.title("Learning Rate Function")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig("Learning_Rate_Function.png")
plt.close()


fig, ax1 = plt.subplots()
ax1.set_xlabel("Epoch")
ax1.set_ylabel("Learning Rate Function")
ax1.plot(hg_lcs[0])

ax2 = ax1.twinx()
ax2.set_ylabel("Learning Rate Function")
ax2.plot(xlrl[0], color='r')

fig.tight_layout()
plt.savefig("Loss_and_Learning.png")

fig,ax = plt.subplots()
for i,o in enumerate(wllow_l):
    ts = ax.bar(x+(i-3)*w, o, w)
    tf = ax.bar(x+(i-3)*w, [nl_l[i][x]-o[x] for x in range(len(o))], w, bottom = o, color='k')
    ts.set_label("xv"+str(i))
tf.set_label("fails")
fig.subplots_adjust(bottom=0.3)
plt.legend(ncol=(len(wllow_l)+1),bbox_to_anchor=(0,-0.3),loc='lower left')
plt.title("Success Rates for Lowest per XValidation, per Class")
plt.xlabel("Number of Paths")
plt.ylabel("% Successful")
plt.savefig("SuccessRateLow.png")

print("\n")
print("Actual Number of Epochs: ",epoch_act)
print("Actual Time: ",time.time()-act_time)
print("FIN")
