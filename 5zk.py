import torch
import torch.nn as nn
import numpy as np
import sys
import os
sys.path.append("/home/et/flow/ftools/")
sys.path.append(os.getcwd())
from libga import *
import class_sort
import matplotlib.pyplot as plt
import torch.optim as optz
from class_sort_gen import *

class GCN(nn.Module):
    def __init__(self):
        super(GCN, self).__init__()
        self.f1 = nn.Linear(34,1)
    def forward(self,x):
        return torch.tanh(self.f1(x))

graphr_file = open("zach_karate_club",'r')
graphrs = graphr_file.read()
graphr_file.close()

graphrs = graphrs[2:-3].split("],[")
graphrs = [list(map(int,i.split(","))) for i in graphrs]
graphrs =[[i-1 for i in j] for j in graphrs]
#
adj = np.matrix(libga(graphrs))     #Adjacency matrix
I = np.eye(len(adj),dtype=np.float32) #Identity
#
## Attraction/Repulsion terms
#Ah = adj+I
Ah = 2*adj-1+I #Times 2 minus one, then plus ones along diagonal
## Attraction/Repulsion matrix
#   > has zeros along the diagonal:                                 points not attracted to themselves
#   > has 1 for indices to which (the row point) is connected:      connected points are attracted
#   > has -1 for for incideces to which it  is not connected:       non-connected points are repelled
##
#
dh = np.array(np.sum(Ah,axis=0))[0]
Ah = np.array(Ah,dtype=np.float32)
dhi = np.matrix(np.diag(dh))**-1
dhi = torch.FloatTensor(dhi.tolist())
ah = torch.from_numpy(Ah)
print(ah)
I = torch.from_numpy(I)
outl = []


import networkx as nx
kg = nx.karate_club_graph()
nodes_l = list(kg.nodes())
nx_labels = [kg.nodes[i]["club"] for i in nodes_l]
clubs_set = list(set(nx_labels))
club_labels = [clubs_set.index(x) for x in nx_labels]

sparse_adj = [list(kg.adj[i]) for i in nodes_l]
tadj = np.zeros((34,34))
for i,o in enumerate(sparse_adj):
    tadj[i][np.array(o)] = 1


net = GCN()
opr = optz.SGD(net.parameters(),lr=0.00002,momentum=0.000000) 
inda = torch.matmul(dhi,ah)

for checkpoint_ind in range(2):
    plt.figure()
    plt.title("Clustering after "+str(checkpoint_ind*250)+" epochs")
    plt.xlabel("Dim 1")
    plt.ylabel("Dim 1")
    outs = net(inda)
    plt.scatter(outs[:,0].tolist(),outs[:,0].tolist())
    for epoch in range(500):
        outs = net(inda)
        #
        #l1 = [torch.sum(((outs[:,0]-outs[:,0][i])**2+(outs[:,1]-outs[:,1][i])**2)*ah[i]) for i in range(len(outs[:,0]))]
        #l1 = [torch.sum(((outs[:,0]-outs[:,0][i])**2)*ah[i]) for i in range(len(outs[:,0]))]
        l1 = torch.sum((outs-outs.T)**2*ah,axis=0)



        #
        ##  Loss function
        #     > Is the sum distance between all points..
        #       multiplied by an attraction/repulsion term.
        #       Therefore, minimizes/maximizes distance 
        #       between attracted/repulsed nodes
        #     > Splits group in two.
        ##  Terms
        #       (out[:,0]-outs[:,0][i])**2 - Distance between all points in x
        #       (out[:,1]-outs[:,1][i])**2 - Distance between all points in y
        #       ah[i]   -   attraction/repulsion term 
        ##

        loss = sum(l1)
        loss.backward()
        opr.step()
        #net.zero_grad() #Doesn't work with zeroing grad
plt.figure()
plt.title("Finished training")
plt.xlabel("Dim 1")
plt.ylabel("Dim 1")
plt.scatter(outs[:,0].tolist(),outs[:,0].tolist())
plt.show()

j=(outs).tolist()
#l=np.round([j[i][0]*2+j[i][1] for i in range(len(j))])

l=np.round([j[i][0] for i in range(len(j))])
#cls = class_sort.cals(l)
#print(cls,set(cls))
k = torch.sign(outs).detach().numpy()
ksl,ksl2,k2 = np.unique(k,axis=0,return_index=True,return_inverse=True)
final_predictions = np.argsort(np.argsort(ksl2))[k2]
##
print(final_predictions.tolist())
print(club_labels)
#print(class_sort_gen(outs)) #class_sort_gen sorts by strings of rounded outputs


    #outl.append(np.sign((outs[0][0]*outs).tolist()).flatten().tolist())
#outs = np.array(outs.T.tolist())
#o = np.array(outl)
#for i in range(20):
#	print(set(o[:,i].tolist()),[o[:,i].tolist().count(j) for j in set(o[:,i].tolist())])
